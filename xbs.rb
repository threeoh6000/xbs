require 'fileutils'

def process (line)
	s = line.split
	if s[0] == "build"
		puts "Building via ewfm."
		runner = `ruby main.rb`
	end
	if s[0] == "run"
		if s.length() != 2
			puts "Invalid amount of arguments at run line."
		else
			puts "Running command " + s[1]
			runner = `#{s[1]}`
		end
	end
	if s[0] == "remove"
		if s.length() != 2
			puts "Invalid amount of arguments at remove line."
		else
			puts "Deleting " + s[1]
			if Dir.exist?(s[1])
				FileUtils.remove_dir(s[1])
			else
				FileUtils.rm(s[1])
			end
		end
	end
	if s[0] == "copy"
		if s.length() != 3
			puts "Invaid amount of arguments at copy line."
		else
			puts "Copying " + s[1]
			FileUtils.cp_r(s[1], s[2])
		end
	end
	if s[0] == "remote"
		if s.length() != 3
			puts "Invalid amount of arguments at remote line."
		else
			puts "Running command " + s[2]+ " on remote host " + s[1]
			runner = `ssh #{s[1]} '#{s[2]}'`
		end
	end
	if s[0] == "upload"
		if s.length() != 3
			puts "Invalid amount of arguments at upload line."
		else
			puts "Uploading " + s[2] + " to remote host " + s[1]
			runner = `scp #{s[2]} #{s[1]}:#{s[2]}`
		end
	end
	if s[0] == "archive"
		if s.length() != 3
			puts "Invalid amount of arguments at archive line."
		else
			puts "Archiving " + s[2] + " with algorithm " + s[1]
			if s[1] == "tar"
				runner = `tar -c #{s[2]} -f #{s[2]}.tar`
			end
			if s[1] == "lzma"
				runner = `lzma -z #{s[2]}`
			end
			if s[1] == "xz"
				runner = `xz -z #{s[2]}`
			end
			if s[1] == "gzip"
				runner = `gzip #{s[2]}`
			end
			if s[1] == "gz"
				runner = `gzip #{s[2]}`
			end
			if s[1] == "compress"
				runner = `compress #{s[2]}`
			end
			if s[1] == "ncompress"
				runner = `compress #{s[2]}`
			end
			if s[1] == "z"
				runner = `compress #{s[2]}`
			end
			if s[1] == "bzip2"
				runner = `bzip2 -z #{s[2]}`
			end
			if s[1] == "bz2"
				runner = `bzip2 -z #{s[2]}`
			end
			if s[1] == "zip"
				runner = `zip #{s[2]}.zip #{s[2]}`
			end
		end
	end
	if s[0] == "move"
		if s.length() != 3
			puts "Invalid amount of arguments at move line."
		else
			puts "Moving " + s[2] + " to " + s[1]
			FileUtils.mv(s[1], s[2])
		end
	end

end

if ARGV[0] == "init"
	if not File.exist?('main.rb')
		puts "ewfm installation not found."
		exit
	end
	if File.exist?('build.xbs')
		puts "xbs already initialised in this installation."
		exit
	end
	puts "Initalising xbs."
	if File.exist?('input/site.conf')
		f = File.new('build.xbs', "w")
		f.syswrite("default")
		FileUtils.mkdir_p("sites/")
		FileUtils.move("input/", "sites/")
		FileUtils.move("sites/input/", "sites/default/")
		b = File.new('sites/default/make.xbs', 'w')
		b.syswrite("build")
	else
		f = File.new('build.xbs', "w")
		FileUtils.mkdir_p("sites/")
	end
	exit
end
if ARGV[0] == "install"
	if not File.exist?('main.rb')
		puts "ewfm installation not found."
		exit
	end
	if ARGV.length() != 2
		puts "Usage: xbs install <theme/processor>"
		exit
	end
	if not File.exist?(ARGV[1])
		puts "Folder not found."
		exit
	end
	if File.exist?(ARGV[1] + "/template.html")
		if File.exist?(ARGV[1] + "/dist")
			puts "Installing theme " + ARGV[1]
			FileUtils.move(ARGV[1], "themes")
		end
		exit
	end
	if File.exist?(ARGV[1] + "/pre.sh")
		puts "Installing processor " + ARGV[1]
		if not File.exist?("proc/")
			FileUtils.mkdir_p("proc/")
		end
		FileUtils.move(ARGV[1], "proc")
		exit
	end
	if File.exist?(ARGV[1] + "/post.sh")
		puts "Installing processor " + ARGV[1]
		if not File.exist?("proc/")
			FileUtils.mkdir_p("proc/")
		end
		FileUtils.move(ARGV[1], "proc")
		exit
	end	
end
if ARGV[0] == "build"
	if not File.exist?('main.rb')
		puts "ewfm installation not found."
		exit
	end

	if ARGV.length() != 1
		puts "Building site " + ARGV[1]
		if File.exist?('input/site.conf')
			puts "There is a site in input, this is being moved."
			FileUtils.move("input", "input-backup")
		end
		if not File.exist?('sites/' + ARGV[1] + '/make.xbs')
			puts ARGV[1] + " either doesn't exist or has no makefile."
			exit
		end
		puts "Beginning build of " + ARGV[1] + "."

		if File.exist?("output/")
			FileUtils.remove_dir("output")
		end
		FileUtils.mkdir_p("output/")
		FileUtils.move("sites/" + ARGV[1], "input/")
		a = IO.readlines("input/make.xbs", chomp: true)
		for line in a
			process(line)
		end
		FileUtils.move("input/", "sites/" + ARGV[1])
		puts "Build complete successfully."
		exit
	end
	if not File.exist?("build.xbs")
		puts "No site inputted and no build.xbs found."
		exit
	else	
		sites = IO.readlines("build.xbs", chomp: true)
		for site in sites
			puts "Building site " + site
			if File.exist?('input/site.conf')
				puts "There is a site in input, this is being moved."
				FileUtils.move("input", "input-backup")
			end
			if not File.exist?('sites/' + site + '/make.xbs')
				puts site + " either doesn't exist or has no makefile."
				exit
			end
			puts "Beginning build of " + site + "."

			if Dir.exist?("output/")
				FileUtils.remove_dir("output")
			end
			FileUtils.mkdir_p("output/")
			FileUtils.move("sites/" + site, "input/")
			a = IO.readlines("input/make.xbs", chomp: true)
			for line in a
				process(line)
			end
			FileUtils.move("input/", "sites/" + site)
			puts "Build complete successfully."
		end
	end
end
if ARGV[0] == "new"
	if not File.exist?('main.rb')
		puts "ewfm installation not found."
		exit
	end
	if ARGV.length() != 2
		puts "Usage: xbs new <name>"
		exit
	end
	dir = "sites/" + ARGV[1]
	FileUtils.mkdir_p("sites/" + ARGV[1])
	f = File.new(dir + '/site.conf', "w")
	f.syswrite("example.com\nNew site\nlinear-base")
	b = File.new(dir + '/inclusions.conf', "w")
	b.syswrite(".jpeg\n.jpg\n.gif\n.png\n.bmp\n.webm\n.webp\n.txt\n.rtf\n.doc\n.xls\n.docx\n.xlsx\n.zip\n.tar\n.gz\n.tgz\n.txz\n.lzma\n.xz\n.bz2\n.bzip2\n.tbz\n.tbz2")
	c = File.new(dir + '/make.xbs', "w")
	c.syswrite("build")
	puts "Site created, please edit the files included in " + dir + "."
end
if ARGV[0] == "help"
	puts "xbs is a build system for ewfm.\ninit - Initialises xbs for your ewfm setup\ninstall <theme/processor> - Installs a theme or processor automatically.\nbuild <optional: site> - builds your sites\nnew <name> - creates a site.\n\nCreated by threeoh6000\newfm.colean.cc"
end
